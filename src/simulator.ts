const SIZE_16K: number = 16384;
const SIZE_32K: number = 32768;
const MAX_16BIT_VAL: number = 65535;
const MINUS_ONE: number = MAX_16BIT_VAL;

export function trim16Bit(x: number): number {
  return x & MAX_16BIT_VAL;
}

export function negBits(x: number): number {
  return (MAX_16BIT_VAL - x) & MAX_16BIT_VAL;
}

export function twoCompliment(x): number {
  return trim16Bit(negBits(x) + 1);
}

const functionMap = {
  0b101010: () => 0,
  0b111111: () => 1,
  0b111010: () => MINUS_ONE,

  0b001100: (opr1, opr2) => opr1,
  0b110000: (opr1, opr2) => opr2,

  0b001101: (opr1, opr2) => negBits(opr1),
  0b110001: (opr1, opr2) => negBits(opr2),

  0b001111: (opr1, opr2) => twoCompliment(opr1),
  0b110011: (opr1, opr2) => twoCompliment(opr2),

  0b011111: (opr1, opr2) => trim16Bit(opr1 + 1),
  0b110111: (opr1, opr2) => trim16Bit(opr2 + 1),

  0b001110: (opr1, opr2) => trim16Bit(opr1 + MINUS_ONE),
  0b110010: (opr1, opr2) => trim16Bit(opr2 + MINUS_ONE),

  0b000010: (opr1, opr2) => trim16Bit(opr1 + opr2),
  0b010011: (opr1, opr2) => trim16Bit(opr1 + twoCompliment(opr2)),

  0b000111: (opr1, opr2) => trim16Bit(twoCompliment(opr1) + opr2),

  0b000000: (opr1, opr2) => opr1 & opr2,
  0b010101: (opr1, opr2) => opr1 | opr2,
};

type MemoryWriteCallback = (addr: number, val: number) => void;

type Code = {
  lineNumber : Number,
  code : String
}

export class HackSimulator {
  RAM: Int32Array;
  ROM: Int32Array;
  PC: number;
  A: number;
  D: number;

  sourceCode: Code[];

  memoryWriteCallbacks: MemoryWriteCallback[];

  constructor() {
    this.RAM = new Int32Array(SIZE_32K);
    this.ROM = new Int32Array(SIZE_32K);
    this.sourceCode = [];
    this.memoryWriteCallbacks = [];

    this.reset();
  }

  reset() {
    this.A = 0;
    this.D = 0;
    this.PC = 0;
  }

  clearRAM() {
    this.RAM = new Int32Array(SIZE_32K);
  }

  loadInstructions(inst: number[]) {
    inst.forEach((v,i) => {
      this.ROM[i] = v;
    });
  }

  isAInstruction(inst: number): boolean {
    return ((inst & 32768) === 0);
  }

  incPC() {
    this.PC += 1;
  }

  decodeCInstruction(inst: number): [number,number,number,number] {
    let a = (inst >> 12) & 1;
    let c = (inst >> 6) & 63;
    let d = (inst >> 3) & 7;
    let j = inst & 7;
    return [a,c,d,j];
  }

  writeMem(addr, value) {
    this.RAM[addr] = value; 
    this.memoryWriteCallbacks.forEach((cb) => {
      cb(addr, value);
    });
  }

  storeResult(dest, result) {
    if ((dest & 0b001) > 0) { 
      this.writeMem(this.A, result);
    }
    if ((dest & 0b010) > 0) { 
      this.D = result; 
    }
    if ((dest & 0b100) > 0) { 
      this.A = result; 
    }
  }

  jump(jmp, result) {
    const zero: boolean = (result === 0);
    const neg: boolean = (result & (1 << 15)) !== 0;
    const pos: boolean = (!neg) && (!zero);

    if ((((jmp & 0b100) > 0) && (neg)) ||
        (((jmp & 0b010) > 0) && (zero)) ||
        (((jmp & 0b001) > 0) && (pos))) {
      this.PC = this.A;
    } else {
      this.incPC();
    }
  }

  executeCInstruction(inst: number) {
    let [a,c,d,j] = this.decodeCInstruction(inst);

    let operand2 = (a === 0) ? this.A : this.RAM[this.A];

    let result = functionMap[c](this.D, operand2);
    result = trim16Bit(result);

    this.storeResult(d, result);
    this.jump(j, result);
  }

  step() {
    let inst = this.ROM[this.PC];

    if (this.isAInstruction(inst)) {
      this.A = inst;
      this.incPC();
    } else {
      this.executeCInstruction(inst);
    }
  }
}

export default HackSimulator;