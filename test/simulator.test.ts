import { expect } from 'chai';
import HackSimulator from '../src/simulator';

import { trim16Bit, negBits, twoCompliment } from '../src/simulator';

describe('Helper functions', () => {
  describe('trim16Bit', () => {
    it('should work with small numbers', () => {
      expect(trim16Bit(0)).to.equal(0);
      expect(trim16Bit(32767)).to.equal(32767);
      expect(trim16Bit(65535)).to.equal(65535);
    });

    it('should work with large numbers', () => {
      expect(trim16Bit(65536)).to.equal(0);
      expect(trim16Bit(98304)).to.equal(32768);
      expect(trim16Bit(131071)).to.equal(65535);
    });
  })  

  describe('negBits', () => {
    it('should work with typical inputs', () => {
      expect(negBits(0b1101)).to.equal(0b1111111111110010);
      expect(negBits(0b1010101010101010)).to.equal(0b0101010101010101);
      expect(negBits(0b1111000011110000)).to.equal(0b0000111100001111);
      expect(negBits(0b0000111100000000)).to.equal(0b1111000011111111);
    });

    it('should trim outputs', () => {
      expect(negBits(0b11101010101010101010)).to.equal(0b0101010101010101);
      expect(negBits(0b01011111000011110000)).to.equal(0b0000111100001111);
      expect(negBits(0b101010000111100000000)).to.equal(0b1111000011111111);
    });
  })
})

describe('HackSimulator', () => {

  let sim: HackSimulator;

  beforeEach(() => {
    sim = new HackSimulator();
  });

  it('should reset', () => {
    sim.reset();
    expect(sim.PC).to.equal(0);
    expect(sim.A).to.equal(0);
    expect(sim.D).to.equal(0);
  });

  it('should load instructions', () => {
    sim.loadInstructions([10,20]);
    expect(sim.ROM[0]).to.equal(10);
    expect(sim.ROM[1]).to.equal(20);
  });

  it('should not clear ROM when reset', () => {
    sim.loadInstructions([10,20]);
    sim.reset();
    expect(sim.ROM[0]).to.equal(10);
    expect(sim.ROM[1]).to.equal(20);
  })

  describe('when executes A-instructions', () => {
    beforeEach(() => {
      sim.loadInstructions([10,20,32767])
    })

    it('should set the A register', () => {
      sim.reset();
      sim.step();
      expect(sim.A).to.equal(10);
      sim.step();
      expect(sim.A).to.equal(20);
      sim.step();
      expect(sim.A).to.equal(32767);
    })
  });

  it('should run correctly when executes add100.asm', () => {
    sim.loadInstructions([
      0b0000000000010000,
      0b1110111111001000,
      0b0000000000010001,
      0b1110101010001000,
      0b0000000000010000,
      0b1111110000010000,
      0b0000000001100100,
      0b1110010011010000,
      0b0000000000010010,
      0b1110001100000001,
      0b0000000000010000,
      0b1111110000010000,
      0b0000000000010001,
      0b1111000010001000,
      0b0000000000010000,
      0b1111110111001000,
      0b0000000000000100,
      0b1110101010000111,
      0b0000000000010010,
      0b1110101010000111,
    ]);

    sim.reset();
    for (let i = 0; i < 2000; i++) {
      sim.step();
    }
    expect(sim.RAM[0b0000000000010001]).to.equal(5050);
  })
});

