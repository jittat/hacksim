"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HackSimulator = void 0;
exports.trim16Bit = trim16Bit;
exports.negBits = negBits;
exports.twoCompliment = twoCompliment;
var SIZE_16K = 16384;
var SIZE_32K = 32768;
var MAX_16BIT_VAL = 65535;
var MINUS_ONE = MAX_16BIT_VAL;
function trim16Bit(x) {
    return x & MAX_16BIT_VAL;
}
function negBits(x) {
    return (MAX_16BIT_VAL - x) & MAX_16BIT_VAL;
}
function twoCompliment(x) {
    return trim16Bit(negBits(x) + 1);
}
var functionMap = {
    42: function () { return 0; },
    63: function () { return 1; },
    58: function () { return MINUS_ONE; },
    12: function (opr1, opr2) { return opr1; },
    48: function (opr1, opr2) { return opr2; },
    13: function (opr1, opr2) { return negBits(opr1); },
    49: function (opr1, opr2) { return negBits(opr2); },
    15: function (opr1, opr2) { return twoCompliment(opr1); },
    51: function (opr1, opr2) { return twoCompliment(opr2); },
    31: function (opr1, opr2) { return trim16Bit(opr1 + 1); },
    55: function (opr1, opr2) { return trim16Bit(opr2 + 1); },
    14: function (opr1, opr2) { return trim16Bit(opr1 + MINUS_ONE); },
    50: function (opr1, opr2) { return trim16Bit(opr2 + MINUS_ONE); },
    2: function (opr1, opr2) { return trim16Bit(opr1 + opr2); },
    19: function (opr1, opr2) { return trim16Bit(opr1 + twoCompliment(opr2)); },
    7: function (opr1, opr2) { return trim16Bit(twoCompliment(opr1) + opr2); },
    0: function (opr1, opr2) { return opr1 & opr2; },
    21: function (opr1, opr2) { return opr1 | opr2; },
};
var HackSimulator = /** @class */ (function () {
    function HackSimulator() {
        this.RAM = new Int32Array(SIZE_32K);
        this.ROM = new Int32Array(SIZE_32K);
        this.sourceCode = [];
        this.memoryWriteCallbacks = [];
        this.reset();
    }
    HackSimulator.prototype.reset = function () {
        this.A = 0;
        this.D = 0;
        this.PC = 0;
    };
    HackSimulator.prototype.clearRAM = function () {
        this.RAM = new Int32Array(SIZE_32K);
    };
    HackSimulator.prototype.loadInstructions = function (inst) {
        var _this = this;
        inst.forEach(function (v, i) {
            _this.ROM[i] = v;
        });
    };
    HackSimulator.prototype.isAInstruction = function (inst) {
        return ((inst & 32768) === 0);
    };
    HackSimulator.prototype.incPC = function () {
        this.PC += 1;
    };
    HackSimulator.prototype.decodeCInstruction = function (inst) {
        var a = (inst >> 12) & 1;
        var c = (inst >> 6) & 63;
        var d = (inst >> 3) & 7;
        var j = inst & 7;
        return [a, c, d, j];
    };
    HackSimulator.prototype.writeMem = function (addr, value) {
        this.RAM[addr] = value;
        this.memoryWriteCallbacks.forEach(function (cb) {
            cb(addr, value);
        });
    };
    HackSimulator.prototype.storeResult = function (dest, result) {
        if ((dest & 1) > 0) {
            this.writeMem(this.A, result);
        }
        if ((dest & 2) > 0) {
            this.D = result;
        }
        if ((dest & 4) > 0) {
            this.A = result;
        }
    };
    HackSimulator.prototype.jump = function (jmp, result) {
        var zero = (result === 0);
        var neg = (result & (1 << 15)) !== 0;
        var pos = (!neg) && (!zero);
        if ((((jmp & 4) > 0) && (neg)) ||
            (((jmp & 2) > 0) && (zero)) ||
            (((jmp & 1) > 0) && (pos))) {
            this.PC = this.A;
        }
        else {
            this.incPC();
        }
    };
    HackSimulator.prototype.executeCInstruction = function (inst) {
        var _a = this.decodeCInstruction(inst), a = _a[0], c = _a[1], d = _a[2], j = _a[3];
        var operand2 = (a === 0) ? this.A : this.RAM[this.A];
        var result = functionMap[c](this.D, operand2);
        result = trim16Bit(result);
        this.storeResult(d, result);
        this.jump(j, result);
    };
    HackSimulator.prototype.step = function () {
        var inst = this.ROM[this.PC];
        if (this.isAInstruction(inst)) {
            this.A = inst;
            this.incPC();
        }
        else {
            this.executeCInstruction(inst);
        }
    };
    return HackSimulator;
}());
exports.HackSimulator = HackSimulator;
exports.default = HackSimulator;
//# sourceMappingURL=simulator.js.map