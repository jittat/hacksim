var SCREEN_ADDR_START = 0x4000;
var SCREEN_ADDR_STOP = 0x6000;
var KEYBOARD_ADDR = 0x6000;

memUpdateCallback = (addr, v) => {
  var valueSpan = document.querySelector("#ramTable tr#ramTable-" + addr + " .memory-values");
  
  if (valueSpan) {
    valueSpan.innerHTML = v;
  }
};

var screenScale = 1;

var updateScreen = (addr, v) => {
  const canvas = document.getElementById('screenCanvas');
  const ctx = canvas.getContext('2d');

  addr -= 0x4000;
  const row = Math.floor(addr / 32);
  const cnk = addr % 32;

  ctx.fillStyle = 'white';
  const rx = cnk * 16 * screenScale;
  const ry = row * screenScale;
  ctx.fillRect(rx, ry, 16 * screenScale, screenScale);

  let b = 1;
  ctx.fillStyle = 'black';
  for (let i = 0; i < 16; i++) {
    if ((v & b) != 0) {
      ctx.fillRect(rx + i*screenScale, ry, screenScale, screenScale);
    }
    b <<= 1;
  }
};

screenUpdateCallback = (addr, v) => {
  if ((addr >= SCREEN_ADDR_START) && (addr < SCREEN_ADDR_STOP)) {
    updateScreen(addr,v);
  }
};

Vue.component('memory', {
  props: {
    name: String,
    tableId: String,
    data: Int32Array,
    displayBase: {
      type: Number,
      default: 2
    },
    isUpdatable: String,
    onRefreshTable: Function
  },
  data: function () {
    return {
      updateAddr: 0,
      currentValue: 0,
      showUpdateBox: false,
      updateError: false,
      updateErrorMessage: ''
    }
  },
  template: 
    `<table class="memory-tables" v-bind:id="tableId">
      <tr><th colspan="2">{{ name }}</th></tr>
      <tr v-for="(item, index) in data" v-bind:id="tableId + '-' + index">
        <td class="idx">{{ index }}</td>
        <td v-on:click="prepareMemoryUpdate(index)" class="values">
          <span class="memory-values" v-if="(!showUpdateBox) || (updateAddr!=index)">{{ item.toString(2) }}</span>
          <span v-else>
            Current value: {{ currentValue }}<br />
            <input v-on:keyup="onKeyUp(index,$event)" class="memory-new-values"></input>
            <button v-on:click.stop="updateMemory(index)">Save</button>
            <button v-on:click.stop="cancelMemoryUpdate(index)">Cancel</button>
            <br />
            <span v-if="updateError">
              {{ updateErrorMessage }}
            </span>
          </span>
        </td>
      </tr>
    </table>`,
  methods: {
    prepareMemoryUpdate: function (addr) {
      if (this.isUpdatable != "true") {
        return;
      }
      if ((this.showUpdateBox) && (this.updateAddr == addr)) {
        return;
      }
      if (!this.showUpdateBox) {
        if (this.onRefreshTable) {
          this.onRefreshTable();
        }
      }

      var valueSpan = document.querySelector("#ramTable tr#ramTable-" + addr + " .memory-values");
      var currentValue = valueSpan.innerHTML;
      this.updateAddr = addr;
      this.currentValue = currentValue;
      this.showUpdateBox = true;
      this.$nextTick(() => {
        var input = document.querySelector("#ramTable tr#ramTable-" + addr + " .memory-new-values");
        input.focus();
      });
    },
    onKeyUp: function (addr, event) {
      if (event.key == 'Enter') {
        this.updateMemory(addr);
      }
    },
    updateMemory: function (addr) {
      if (this.isUpdatable != "true") {
        return;
      }
      var newValueInput = document.querySelector("#ramTable tr#ramTable-" + addr + " .memory-new-values");
      var newValue = newValueInput.value;

      if ((newValue !== null) && (/[0-9]+/.test(newValue.trim()))) {
        newValue = parseInt(newValue.trim());
        if ((newValue >= 0) && (newValue <= 65535)) {
          var simulator = this.$parent.simulator;
          simulator.writeMem(addr, newValue);    
          this.showUpdateBox = false;
          this.updateError = false;
          this.updateAddr = -1;
          if (this.onRefreshTable) {
            this.onRefreshTable();
          }
        } else {
          this.updateError = true;
          this.updateErrorMessage = `Error: Number out of range (${newValue})`;
        }
      } else {
        if (newValue !== null) {
          this.updateError = true;
          this.updateErrorMessage = `Error: Number format error (${newValue})`;
        }
      }
    },
    cancelMemoryUpdate: function (addr) {
      if (this.isUpdatable != "true") {
        return;
      }
      this.showUpdateBox = false;
      this.updateError = false;
      this.updateAddr = -1;
    }
  }
});

Vue.component('keyboard', {
  props: [
    'onKeyValueChange'
  ],
  data: function() {
    return {
      keyValue: 0,
      keyMaps: [
        { label: 'left', value: 130 },
        { label: 'up', value: 131 },
        { label: 'down', value: 133 },
        { label: 'right', value: 132, br: true },
        { label: 'space', value: 32 },
        { label: 'enter', value: 128 }
      ]
    };
  },
  template:
  `<div>
    Keyboard: {{ keyValue }}
    <br/>
    <span v-for="k in keyMaps">
      <button v-on:mousedown="onMouseDown(k.value)" v-on:mouseup="onMouseUp" v-on:mouseleave="onMouseLeave">{{ k.label }}</button>
      <br v-if="k.br"/>
    </span>
  </div>`,
  methods: {
    onMouseDown: function (value) {
      if (this.keyValue != value) {
        this.onKeyValueChange(value);
      }
      this.keyValue = value;
    },
    onMouseLeave: function(event) {
      if (this.keyValue != 0) {
        this.onKeyValueChange(0);
      }
      this.keyValue = 0;
    },
    onMouseUp: function (event) {
      if (this.keyValue != 0) {
        this.onKeyValueChange(0);
      }
      this.keyValue = 0;
    }
  }
});

var simulationIntervalID = null;

Vue.component('simulator', {
  props: {
    simulator: Object,
    memoryDisplaySlots: {type: Number, default: 30},
    screenScale: {type: Number, default: 1},
    lightWeight: {
      type: Boolean,
      default: false
    },
    fastestSteps: {
      type: Number,
      default: 103
    }
  },
  data: function () {
    return {
      simStarted: false,
      stepsPerInterval: 1,
      keyValue: 0,
      ramUpdateKey: 1,
      ramComputedPropertyMagic: 0,
      romMode : true,
    }
  },
  template: 
  `<div class="simulator">
    <div class="simulator-header">
      <b>HACK Simulator</b>
      <span v-if="simStarted">
        [Started]
        <button v-on:click="stopSimulation">Stop</button>
      </span>
      <span v-else>
        <button v-on:click="reset">Reset</button>
        <button v-on:click="clearRAM">Clear RAM</button>
        <button v-on:click="step">Step</button>
        <button v-on:click="startSlow">Start slow</button>
        <button v-on:click="startFast">Start fast</button>
        <button v-on:click="startFaster">Start faster</button>
        <button v-on:click="startFastest">Start fastest</button>
        <button v-if="simulator.sourceCode.length"v-on:click="toggleROM">toggle ROM</button>
      </span>
    </div>
    <div>
      <div v-if="!lightWeight">
        <div v-if="!romMode">
        <div style="float: left;" class="code-block">
          <div v-for="(line,index) in simulator.sourceCode" v-bind:id="'line-' + (Number.isInteger(line.lineNumber) ? line.lineNumber : (index + 'n'))">
            <p>{{ line.code }} </p>
          </div>
        </div>
        </div>
        <div v-else>
        <div style="float: left; margin-left: 10px;">
          <memory name="ROM" table-id="romTable" v-bind:data="romFront" is-updatable="false" />
        </div>
        </div>
        <div style="float: left; margin-left: 10px;">
          <memory 
            :key="ramUpdateKey" 
            name="RAM" 
            table-id="ramTable" 
            v-bind:data="ramFront"
             
            is-updatable="true"
            v-bind:on-refresh-table="onRAMRefresh" />
        </div>
      </div>
      <div style="float: left; margin-left: 10px;">
        <div style="float:left; margin-right: 10px;" v-if="!lightWeight">
          <table class="memory-tables">
            <tr><th colspan="2">Registers</th></tr>
            <tr>
              <td class="idx">PC:</td>
              <td ref="regPC" class="values" id="registerPCValue">{{ simulator.PC }}</td>
            </tr>
            <tr>
              <td class="idx">A:</td>
              <td ref="regA" class="values" id="registerAValue">{{ simulator.A }}</td>
            </tr>
            <tr>
              <td class="idx">D:</td>
              <td ref="regD" class="values" id="registerDValue">{{ simulator.D }}</td>
            </tr>
          </table>
        </div>
        <div style="float: left;">
          <keyboard v-bind:on-key-value-change="onKeyValueChange"/>
        </div>
        <br style="clear: both;"/>
        <div>
          Screen:<br />
          <canvas id="screenCanvas" v-bind:width="screenScale * 512" v-bind:height="screenScale * 256" style="border: 1px solid gray;"></canvas>
        </div>
      </div> 
    </div>
  </div>`,

  mounted: function() {
    this.highlightPC();
    screenScale = this.screenScale;
  },

  computed: {
    romFront: function () {
      return this.simulator.ROM.slice(0, this.memoryDisplaySlots);
    },
    ramFront: function () {
      this.ramComputedPropertyMagic;
      var res = this.simulator.RAM.slice(0, this.memoryDisplaySlots);
      return res;
    }
  },

  methods: {
    highlightPC: function () {
      let oldElt = document.querySelector("#romTable tr.hl");
      if (oldElt) {
        oldElt.classList.remove("hl");
      }
      let pc = this.simulator.PC;
      let romTable = document.querySelector("#romTable");
      if (romTable) {
        document.querySelector("#romTable tr#romTable-" + pc)
        .classList.add(!this.romMode ? "marked" : "hl");
      }
      //scroll to view marked element
      var container = document.querySelector('.code-block');
      if(!container) return;
      var elements = container.querySelectorAll('.marked');
      elements.forEach(function(element) {
        element.classList.remove('marked');
      });
      var markedElement = container.querySelector('#line-' + pc);
      console.log(markedElement);

      if (markedElement) {
          markedElement.classList.add('marked');
          // container.scrollTop = markedElement.offsetTop - container.clientHeight / 2 + markedElement.clientHeight / 2;
          // container.scrollLeft = markedElement.offsetLeft - container.clientWidth / 2 + markedElement.clientWidth / 2;
          markedElement.scrollIntoView({behavior: "smooth", block: "nearest"});
      }
    },

    step: function (event) {
      this.simulator.step();
      this.highlightPC();
    },

    reset: function (event) {
      this.simulator.reset();
      this.highlightPC();
    },

    clearRAM: function (event) {
      this.simulator.clearRAM();
      this.ramUpdateKey += 1;
      this.clearScreen();
    },

    onRAMRefresh: function () {
      this.ramComputedPropertyMagic += 1;
    },

    clearScreen: function () {
      const canvas = document.getElementById('screenCanvas');
      const ctx = canvas.getContext('2d');

      ctx.fillStyle = 'white';
      ctx.fillRect(0, 0, 1024, 512);
    },

    onKeyValueChange: function (value) {
      this.keyValue = value;
      this.simulator.writeMem(KEYBOARD_ADDR, this.keyValue);
    },

    startSimulation: function(delay) {
      this.simStarted = true;
      simulationIntervalID = setInterval(() => {
        if (this.stepsPerInterval == 1) {
          this.step();
        } else {
          for (let i = 0; i < this.stepsPerInterval; i++) {
            this.simulator.step();
          }
          if (!this.lightWeight) {
            this.highlightPC();
          }
        }
      },delay);
    },

    startSlow: function () {
      this.startSimulation(100);
    },
    startFast: function () {
      this.startSimulation(10);
    },
    startFaster: function () {
      this.startSimulation(0);
    },
    startFastest: function () {
      this.stepsPerInterval = this.fastestSteps;
      this.startSimulation(0);
    },
    toggleROM: function () {
      this.romMode = !this.romMode;
      // can't directly call highlightPC() here, since hightlightPC() is done before the ROM 
      // could rerender, thus the highlight must be delayed such that highlightPC() is done 
      // after the rerender of the ROM
      setTimeout(() => {
        this.highlightPC();
      }, 100)
    },

    stopSimulation: function () {
      if (simulationIntervalID) {
        clearInterval(simulationIntervalID);
        simulationIntervalID = null;
      }
      this.stepsPerInterval = 1;
      this.simStarted = false;
    },
  }
});
